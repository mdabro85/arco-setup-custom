#! /bin/bash

nitrogen --save .

cd ~
sudo pacman -S --needed - <~/arco-setup-custom/package_list.txt
paru -S --needed - <~/arco-setup-custom/package_list.txt
paru -S --needed dmscripts-git

sudo rm -rf ~/.config/qtile ~/.config/alacritty ~/.config/starship ~/shells ~/.config/nvim ~/.config/sddm ~/.config/dmscripts

git clone https://gitlab.com/mdabro85/qtile.git
mv ~/qtile ~/.config

git clone https://gitlab.com/mdabro85/alacritty.git
mv ~/alacritty ~/.config

curl -sS https://starship.rs/install.sh | sh
git clone https://gitlab.com/mdabro85/starship.git
mv ~/starship ~/.config

git clone https://gitlab.com/mdabro85/shells.git
cp ~/shells/.bashrc ~/.bashrc
cp ~/shells/.bashrc-personal ~/.bashrc-personal

git clone https://gitlab.com/mdabro85/nvim.git
mv ~/nvim ~/.config

git clone https://gitlab.com/mdabro85/dmscripts.git
sudo mv ~/dmscripts ~/.config

git clone https://gitlab.com/mdabro85/sddm.git
sudo mv ~/sddm ~/.config
